import Http from '../../utils/http'
import Config from '../../config'

const state = {
  apiUrl: Config.apiUrl,
  currentPageHeader: '',
  message: null,
  error: null,
  errorDetail:[],

}
const getters = {
  apiUrl: state => state.apiUrl,
  currentPageHeader: state => state.currentPageHeader,
  message: state => state.message,
  error: state => state.error,
  errorDetail: state => state.errorDetail,
}

const mutations = {
  setMessage: (state, message) => {
    state.message = message
  },
  setError: (state, error) => {
    state.errorDetail = []
    state.error = null

    if (error && error.response && error.response.data && error.response.data.error && error.response.data.error.detail != null)  {
      state.errorDetail = error.response.data.error.detail 

      return
    }

    if (error && error.response && error.response.data && error.response.data.error) {
      state.error = error.response.data.error.message
      
      return
    }

    
    state.error = error
  },
  setCurrentPageHeader: (state, header) => {
    state.currentPageHeader = header
  },
}

const actions = {
  setDictionary: ({commit}) => {
    return new Promise((resolve) => {
      Http.get(state.apiUrl + '/dictionaries/45')
        .then(resp => {
          commit('users/setStatuses', resp.data.userState, {root:true})
          resolve(resp)
        })
    })
  },
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
