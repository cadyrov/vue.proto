import Http from '../../utils/http'
import Config from '../../config'

const state = {
  data: [],
  statuses: [],
  page: null,
  total: null,
  limit: null,
}
const getters = {
  data: state => state.data,
  statuses: state => state.statuses,
  pagination: state => {
    return {total: state.total, limit: state.limit, page: state.page}
  }
}

const mutations = {
  setData: (state, data) => {
    state.data = data
  },
  setStatuses: (state, statuses) => {
    state.statuses = statuses
  },
  setEntity:(state, entity) => {
    state.data.map((e, i) => {
      if (e.id == entity.id) {
        state.data[i] = entity
      }
    })
  },
  pagination:(state, pagination) => {
    state.page = pagination.page ? pagination.page : null
    state.total = pagination.total ? pagination.total : null
    state.limit = pagination.limit ? pagination.limit : null
  },
}

const actions = {
  find: ({commit}, payload) => {
    return new Promise((resolve) => {
      Http.post(Config.apiUrl+ '/v1/user', payload)
        .then(resp => {
          commit('setData', resp.data)
          commit('pagination', resp.pagination)
          resolve(resp)
        })
    })
  },
  create:({commit}, payload) => {
    return new Promise((resolve, reject) => {
      Http.post(Config.apiUrl+ '/v1/user/create', payload)
        .then(resp => {
          commit('setEntity', {})
          resolve(resp)
        })
        .catch((e) => {
          reject(e)
        })
    })
  },
  delete:({commit}, payload) => {
    return new Promise((resolve, reject) => {
      Http.delete(Config.apiUrl+ '/v1/user/' + payload.id)
        .then(resp => {
          commit('setEntity', {})
          resolve(resp)
        })
        .catch((e) => {
          reject(e)
        })
    })
  },
  restore:({commit}, payload) => {
    return new Promise((resolve, reject) => {
      Http.put(Config.apiUrl+ '/v1/user/' + payload.id)
        .then(resp => {
          commit('setEntity', {})
          resolve(resp)
        })
        .catch((e) => {
          reject(e)
        })
    })
  },
  update:({commit}, payload) => {
    return new Promise((resolve) => {
      Http.patch(Config.apiUrl+ '/v1/user/' + payload.id , payload)
        .then(resp => {
          commit('setEntity', resp.data)
          resolve(resp)
        })
    })
  },
  addRole: ({dispatch}, roleName) => {
    return new Promise((resolve) => {
      Http.post(state.apiUrl + 'role/create', {name: roleName, description: roleName})
        .then(resp => {
          dispatch('getRoles')
          resolve(resp)
        })
    })
  },
  dropRole: ({dispatch}, name) => {
    return new Promise((resolve) => {
      Http.post(state.apiUrl + 'role/delete', {name: name})
        .then(resp => {
          dispatch('getRoles')
          resolve(resp)
        })
    })
  },
  upload ({dispatch},payload) {
    return new Promise((resolve, reject) => {
      Http.post(Config.apiUrl + '/v1/user/img', payload)
        .then(function (response) {
            dispatch('auth/check',{},  {root:true})
            resolve(response)
        })
        .catch(err => {
            reject(err)
        })
    })
  },
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
