import Http from '../../utils/http'
import Config from '../../config'

const state = {
  isAuthenticated: null,
  user: {},
}

const getters = {
  isAuthenticated: state => state.isAuthenticated,
  user: state => state.user
}

const actions = {
  auth: ({commit, dispatch}, LoginForm) => {
    return new Promise((resolve) => {
      commit('app/setError', null, {root: true})
      Http.post(Config.apiUrl + '/v1/auth', LoginForm)
        .then(resp => {
          commit('app/setError', null, {root: true})
          commit('setIsAuthenticated', true)
          dispatch('check')
          resolve(resp)
        })
        .catch(() => {
          commit('app/setError', "Неверный логин и пароль", {root: true})
          commit('setIsAuthenticated', false)
        })
    })
  },
  changePassword: ({commit}, passForm) => {
    return new Promise((resolve) => {
      commit('app/setError', null, {root: true})
      Http.post(Config.apiUrl + '/v1/changepassword', passForm)
        .then(resp => {
          commit('app/setMessage', 'Пароль успешно изменен', {root: true})
          resolve(resp)
        })
        .catch(() => {
          commit('app/setError', "Неверные пароли", {root: true})
        })
    })
  },
  registry: ({commit}, email) => {  
    return new Promise((resolve,reject) => {
      Http.post(Config.apiUrl + '/v1/auth/registry', email)
        .then((resp) => {
          commit('app/setMessage', 'Проверьте вашу почту', {root: true})
          resolve(resp)
        }).catch((err) => {
          commit('app/setError', err, {root: true})
          reject(err)
      })
    })
  },
  requestpassword: ({commit}, email) => {
    return new Promise((resolve) => {
      Http.post(Config.apiUrl + '/v1/auth/requestpassword', email)
        .then((resp) => {
          commit('app/setMessage', 'Проверьте вашу почту')
          resolve(resp)
        })
        .catch(() => {
          commit('app/setError', "Неверный логин", {root: true})
        })
    })
  },
  check: ({commit}) => {
    return new Promise((resolve, reject) => {
      Http.get(Config.apiUrl + '/v1/check')
        .then(resp => {
          commit('setIsAuthenticated', true)
          commit('setUser', resp.data)
          resolve(resp)
        })
        .catch(err => {
          if (err.response) {
            commit('setIsAuthenticated', false)
            reject('error')
          }
        })
    })
  },
  activate: ({commit}, payload) => {
    return new Promise((resolve) => {
      Http.post(Config.apiUrl + '/v1/auth/activate', payload, {withCredentials: true})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          if (err.response) {
            commit('app/setError',"some error", {root: true})
          }
        })
    })
  },
  logout: ({commit, dispatch}) => {
    return new Promise((resolve) => {
      Http.get(Config.apiUrl + '/v1/logout', {withCredentials: true})
        .then(resp => {
          commit('setIsAuthenticated', false)
          dispatch('check')
          resolve(resp)
        })
    })
  },
}


const mutations = {
  setIsAuthenticated: (state, isAuthenticated) => {
    state.isAuthenticated = isAuthenticated
  },
  setUser: (state, user) => {
    state.user = user
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
