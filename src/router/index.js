import Vue from 'vue'
import store from '../store'
import Router from 'vue-router'
import Login from '../components/admin/login'
import Logout from '../components/admin/logout'
import Users from '../components/admin/users'
import Profile from '../components/admin/users/profile'
import RightUser from '../components/admin/users/right'
import Restore from '../components/admin/restore'
import Activate from '../components/admin/activate'
import Dashboard from '../components/dashboard'
import Registration from '../components/admin/registration'


Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (store.state.auth.isAuthenticated !== true) {
    next()
    
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.state.auth.isAuthenticated === true) {
    next()
    return
  }
  next('/login')
}


const check = (to, from, next) => {
  store.state.app.message=null
  store.state.app.error=null 
  store.state.app.errorDetail = [] 
  store.dispatch('auth/check')
    .then(() => {
      next()
    })
    .catch(error => {
      console.log('error: ', error)
      next('/login')
    })
  next()
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Dashboard,
      beforeEnter: check, ifAuthenticated,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: check, ifNotAuthenticated,
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration,
      beforeEnter: ifNotAuthenticated,
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      beforeEnter: check, ifAuthenticated,
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: check, ifAuthenticated,
    },
    {
      path: '/rights/:id',
      name: 'rightsUser',
      component: RightUser,
      beforeEnter: check, ifAuthenticated,
    },
    {
      path: '/restore',
      name: 'restore',
      component: Restore,
    },
    {
      path: '/activate/:id',
      name: 'activate',
      component: Activate,
    },
  ],
})
